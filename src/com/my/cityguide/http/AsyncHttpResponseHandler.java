package com.my.cityguide.http;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.util.EntityUtils;

/**
 * This class is used to handle responses for the requests
 * @author xichen
 *
 */

public abstract class AsyncHttpResponseHandler {
	
	public AsyncHttpResponseHandler(){
		
	}
	
	public abstract void onSuccess(int responseCode, String response);
    public abstract void onFailure(int statusCode, Throwable error, String content);

	protected void handleResponse(HttpResponse response) {
		//TODO: implement this method to handle HTTP response
		
		StatusLine status = response.getStatusLine();
		
		int statusCode = status.getStatusCode();
		String responseBody = null;
		
		HttpEntity httpEntity = response.getEntity();
		
		if(httpEntity!=null){
			try {
				httpEntity = new BufferedHttpEntity(httpEntity);
				responseBody = EntityUtils.toString(httpEntity, "UTF-8");
			} catch (IOException e) {
				onFailure(-1, e, null);
				e.printStackTrace();
				return;
			}
		}
		
		if(statusCode == 401){
			//TODO Handle error
		}else{
			onSuccess(statusCode, responseBody);
		}
		
	}

}
