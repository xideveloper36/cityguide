package com.my.cityguide.http;

import org.apache.http.HttpVersion;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

import roboguice.inject.ContextSingleton;
import android.util.Log;

/**
 * This class configures the Apache HTTP client
 * @author xichen
 *
 */

@ContextSingleton
public class HttpClientConfiguration {
	private static final int MAX_CONNECTIONS = 10;
	private static final int SOCKET_TIMEOUT = 15 * 1000;
	private static final int SOCKET_BUFFER_SIZE = 8192;
	
	private final DefaultHttpClient mHttpClient;
	
	public HttpClientConfiguration() {
		Log.v("HttpClientConfiguration-Constructor","ok");
		BasicHttpParams httpParams = new BasicHttpParams();
        ConnManagerParams.setTimeout(httpParams, SOCKET_TIMEOUT);
        ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRouteBean(MAX_CONNECTIONS));
        ConnManagerParams.setMaxTotalConnections(httpParams, MAX_CONNECTIONS);

        HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT);
        HttpConnectionParams.setConnectionTimeout(httpParams, SOCKET_TIMEOUT);
        HttpConnectionParams.setConnectionTimeout(httpParams, SOCKET_TIMEOUT);
        HttpConnectionParams.setTcpNoDelay(httpParams, true);
        HttpConnectionParams.setSocketBufferSize(httpParams, SOCKET_BUFFER_SIZE);
        
        HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(httpParams, "utf-8");
        
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        
        //assume backend uses Verisign certificate which is trusted by default
        registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        
      //@ThreadSafeClientConnManager manages a pool of client connections and is able to service connection requests from multiple execution threads.
        this.mHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, registry), httpParams);
	}
	
	public DefaultHttpClient getHttpClient() {
		return mHttpClient;
	}
}
