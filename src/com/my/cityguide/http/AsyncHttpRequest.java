package com.my.cityguide.http;

import java.io.IOException;
import java.net.ConnectException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.AbstractHttpClient;

import android.util.Log;

public class AsyncHttpRequest implements Runnable{
	
	private final AbstractHttpClient client;
    private final HttpUriRequest request;
    private final AsyncHttpResponseHandler responseHandler;

    public AsyncHttpRequest(AbstractHttpClient client, HttpUriRequest request, AsyncHttpResponseHandler responseHandler) {
        this.client = client;
        this.request = request;
        this.responseHandler = responseHandler;
    }

    @Override
    public void run() {
        try {
            makeRequest();
        } catch (IOException e) {
        	Log.v("*AsyncHttpRequest FAILURE -1*","Failue -1");
            responseHandler.onFailure(-1, e, null);
        }
    }

    private void makeRequest() throws ConnectException
    {
    	
        IOException cause = null;
        try {
        	
        	if(!Thread.currentThread().isInterrupted()) {
        		
        		HttpResponse response = client.execute(request);
        		
        		if(!Thread.currentThread().isInterrupted()) {
        			
        			if(responseHandler != null) {
        				
        				responseHandler.handleResponse(response);
        			
        			}
        		} else{
        			
        			Log.v("*Make requestXXX*","ok");
    
        			//TODO: this block is reached whenever the request is cancelled before its response is received
        		}
        	}
            return;
        } catch (IOException e) {
        	Log.v("*Make requestException*","ok");
            cause = e;
        } catch (NullPointerException e) {
        	
        	cause = new IOException("NullPonterException in HttpClient" + e.getMessage());
        		
        } 
        
        ConnectException ex = new ConnectException();
        ex.initCause(cause);
        throw ex;
    }

}
