package com.my.cityguide.http;

import java.net.URI;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import roboguice.inject.ContextSingleton;
import android.util.Log;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * HTTP client class
 * @author xichen
 *
 */

@ContextSingleton
public class AsyncHttpClient {
	
	private final DefaultHttpClient mHttpClient;
    private final ThreadPoolExecutor mThreadPool;

	
	 /**
     * Create a new AsyncHttpClient.
     */
    @Inject
    public AsyncHttpClient(HttpClientConfiguration httpClientConfig) {
    	Log.v("*AsyncHttpClient constructor*","ok");
    	//create thread pool from Executors factory
    	this.mThreadPool = (ThreadPoolExecutor)Executors.newCachedThreadPool();
    	//this http client is created with 'ThreadSafeClientConnManager' which manages a connection pool & limits the nr of connections
		this.mHttpClient = httpClientConfig.getHttpClient();
    } 
    
    /**
     * Perform a HTTP GET request without any parameters.
     * @param url: the URL to send the request to.
     * @param responseHandler: the response handler instance that should handle the response.
     */
    public void get(String url, AsyncHttpResponseHandler responseHandler) {
        sendRequest(mHttpClient, new HttpGet(url), null, responseHandler);
    }

    //TODO: implement the http post method
    public void post(){
    	
    }
    

    //TODO: implement the http put method
    public void put(){
    	
    }

    //TODO: implement the http delete method
    public void delete(){
    	
    }
    
    
    private void sendRequest(DefaultHttpClient client, HttpUriRequest uriRequest, String contentType, AsyncHttpResponseHandler responseHandler) {

       // uriRequest.addHeader("User-Agent", "Android");
       // uriRequest.addHeader("X-API", "ANDROID/"+"1.2.0"+"/"+"1.1.0");

        mThreadPool.submit(new AsyncHttpRequest(client, uriRequest, responseHandler));
    }
    
    private void setBody(String body, HttpEntityEnclosingRequestBase request) {
		if (body != null && body.length() > 0) {
			try {
				StringEntity se = new StringEntity(body, "UTF-8");
				request.setEntity(se);
			} catch (Exception e) {
				Log.e("Post Exception:", ""+e.getMessage());
				return;
			}
		}
	}
	
	// http://stackoverflow.com/questions/3773338/httpdelete-with-body
	private static class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
	    public static final String METHOD_NAME = "DELETE";
	    public String getMethod() { return METHOD_NAME; }

	    public HttpDeleteWithBody(final String uri) {
	        super();
	        setURI(URI.create(uri));
	    }
	}
   
}
