package com.my.cityguide.aidl;

interface IFoodService {
	int getPid();
	String getFood();
	void callFood(String name, boolean isHot);
}