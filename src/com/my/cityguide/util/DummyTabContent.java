package com.my.cityguide.util;

import android.content.Context;
import android.view.View;
import android.widget.TabHost.TabContentFactory;

public class DummyTabContent implements TabContentFactory{
	
	private Context context;
	
	public DummyTabContent(Context context){
		this.context = context;
	}
	
	@Override
	public View createTabContent(String tag) {
		View v = new View(context);
        return v;
	}

}
