package com.my.cityguide.util;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.my.cityguide.R;
import com.my.cityguide.data.HotelDTO;

public class HotelsAdapter extends BaseAdapter{
	
	private Context context;
	private List<HotelDTO> hotels;
	private int rowLayoutResource;
	
	public HotelsAdapter(Context context, List<HotelDTO> hotels, int rowLayoutResource){
		this.context = context;
		this.hotels = hotels;
		this.rowLayoutResource = rowLayoutResource;
	}
	
	@Override
	public int getCount(){
		if(hotels!=null){
			return hotels.size();
		}else{
			return 0;
		}
	}
	
	@Override
	public Object getItem(int position) {
		return hotels.get(position);
	}

	@Override
	public long getItemId(int position) {
		return hotels.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View row = convertView;
		HotelHolder holder = null;
		
		if(row == null){
			
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(rowLayoutResource, parent, false);
            
            holder = new HotelHolder();
            holder.name = (TextView) row.findViewById(R.id.hotel_name);
            holder.price = (TextView) row.findViewById(R.id.hotel_price);
            holder.rowLine = (View) row.findViewById(R.id.hotel_row_line);
            row.setTag(holder);
		}else{
			
			holder = (HotelHolder) row.getTag();
		}
		
		
		HotelDTO hotel = hotels.get(position);
		
		
		holder.name.setText(hotel.getName());
		holder.price.setText(hotel.getPrice()+"€");
		
		return row;
	}
	
	static class HotelHolder
    {
        TextView name;
        TextView price;
        View rowLine;
    }

}
