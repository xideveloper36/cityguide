package com.my.cityguide.util;

import roboguice.inject.ContextSingleton;

import com.google.inject.Inject;
import com.my.cityguide.http.AsyncHttpClient;
import com.my.cityguide.protocol.BannerAdReq;
import com.my.cityguide.protocol.GetHotelsReq;
import com.my.cityguide.protocol.GetServiceInfoReq;

/**
 * A place to inject objects which will only be involved via events.
 * The injection is required to instantiates the object.
 * 
 * (This EventObservers class is injected in Activity)
 * 
 * @author xichen
 *
 */

@ContextSingleton
public class EventObservers {
	
	@Inject AsyncHttpClient httpClient;
	@Inject BannerAdReq bannerAdReq;
	@Inject GetHotelsReq getHotelsReq;
	@Inject GetServiceInfoReq getServiceInfoReq;
	

}
