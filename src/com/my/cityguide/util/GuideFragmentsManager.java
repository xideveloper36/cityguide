package com.my.cityguide.util;

import java.util.HashMap;
import java.util.Map;

import roboguice.event.EventThread;
import roboguice.event.Observes;
import roboguice.inject.ContextSingleton;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.my.cityguide.CityGuideActivity;
import com.my.cityguide.R;
import com.my.cityguide.event.NewSessionEvent;
import com.my.cityguide.fragments.AbstractGuideFragment;
import com.my.cityguide.fragments.FrontpageFragment;

@ContextSingleton
public class GuideFragmentsManager {

	private CityGuideActivity activity;
	
	private Map<Class<?>, AbstractGuideFragment> pages = new HashMap<Class<?>, AbstractGuideFragment>();

	
	public void setActivity(CityGuideActivity activity) {
		this.activity = activity;
	}
	
	public CityGuideActivity getActivity(){
		return activity;
	}
	
	public void onNewSession(@Observes(EventThread.UI) NewSessionEvent evt) {
		Log.v("Observe new session","ok");
		setCurrentFragment(FrontpageFragment.class);
	}
	
	public void updateFragmentAsTabContent(String[] fragTags, Class<? extends AbstractGuideFragment>[] fragmentClasses, String tabId, int tabContentPlaceholder){
		FragmentManager fragmentManager = activity.getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		for(int i=0; i<fragTags.length; i++){
			
			String fragTag = fragTags[i];
			AbstractGuideFragment frag = (AbstractGuideFragment) fragmentManager.findFragmentByTag(fragTag);
			if(frag!=null)
				fragmentTransaction.detach(frag);
			
			
			
			
			if(tabId.equalsIgnoreCase(fragTag)){
				if(frag==null){
					/*Create the fragment and adding to fragmentTransaction*/
					fragmentTransaction.add(tabContentPlaceholder, getFragment(fragmentClasses[i].getName()), fragTag);
				}else{
					/*Bring to the front, if already exists in the fragmentTransaction*/
					fragmentTransaction.attach(frag);
				}	
			}
		}//end of for loop
		
		fragmentTransaction.commit();
		
	}
	
	
	public void updateSpecificFragmentAsTabContent(String fragTag, Class<? extends AbstractGuideFragment> fragmentClass, String tabId, int tabContentPlaceholder){
		FragmentManager fragmentManager = activity.getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		
		AbstractGuideFragment frag = (AbstractGuideFragment) fragmentManager.findFragmentByTag(fragTag);
		if(frag!=null)
			fragmentTransaction.detach(frag);
			
			
			
			
		if(tabId.equalsIgnoreCase(fragTag)){
			if(frag==null){
				/*Create the fragment and adding to fragmentTransaction*/
				fragmentTransaction.add(tabContentPlaceholder, getFragment(fragmentClass.getName()), fragTag);
			}else{
				/*Bring to the front, if already exists in the fragmentTransaction*/
				fragmentTransaction.attach(frag);
			}	
		}

		
		fragmentTransaction.commit();
		
	}
	
	
	
	
	
	public void setCurrentFragment(Class<? extends AbstractGuideFragment> fragmentClass) {
		FragmentManager fragmentManager = activity.getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		AbstractGuideFragment frag = getFragment(fragmentClass.getName());
		
		fragmentTransaction.add(R.id.fragment_placeholder, frag, fragmentClass.getName());
		fragmentTransaction.commit();
	}
	
	
	public void changeToFragment(Class<? extends AbstractGuideFragment> fragmentClass,
			boolean putOnStack, Bundle bundle) {
		
		String fragmentName = fragmentClass.getName();
		changeToFragment(fragmentName, putOnStack, bundle);
		
	}
	
	private void changeToFragment(String fragmentName, boolean putOnStack, Bundle bundle) {
		
		FragmentManager fragMgr = activity.getSupportFragmentManager();
		FragmentTransaction fragTrans = fragMgr.beginTransaction();
		AbstractGuideFragment fragment = getFragment(fragmentName);
		
		Log.v("*On Change frag*", fragment.getClass().getName());
		
		passArgumentsToFragment(fragment, bundle);
		fragTrans.replace(R.id.fragment_placeholder, fragment, fragmentName);
		fragTrans.addToBackStack(null);
    	
		fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		int transId = fragTrans.commit();
		
		Log.v("*Transaction ID is*", String.valueOf(transId));
	
		fragMgr.executePendingTransactions();
		
	}
	
	private void passArgumentsToFragment(AbstractGuideFragment fragment, Bundle bundle) {
		
		try {
			fragment.setArguments(bundle); 
		} catch (IllegalStateException ie) {
			Log.w(getClass().getName(), "Tried to set arguments of already active bundle "+fragment.getClass().getName());
		}
	}
	
	
	public AbstractGuideFragment getFragment(String name) {
		try {
			Class<?> pageClass = Class.forName(name);
			AbstractGuideFragment page = pages.get(pageClass);
			if (page == null) {
				Log.d("GuideFragmentsManager", "making new "+name);
				page = (AbstractGuideFragment)pageClass.newInstance();
				pages.put(pageClass, page);
			}
			return page;
		} catch (ClassNotFoundException ce) {
			throw new IllegalArgumentException("No such page: "+name, ce);
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to construct page "+name, e);
		}
	}
}
