package com.my.cityguide.util;

import android.content.Context;
import android.content.pm.PackageInfo;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Provides at runtime the version of the application, defined in versionName in the application manifest.
 * @author xichen
 *
 */

public class AppVersionProvider implements Provider<String>{
	
	@Inject Context context;
	
	@Override
	public String get() {
		try {
			PackageInfo pkg = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return pkg.versionName;
		} catch (Exception e) {
			throw new IllegalStateException("Unable to obtain app version", e);
		}
	}

}
