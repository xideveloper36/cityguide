package com.my.cityguide.event;

import com.my.cityguide.data.BannerAdDTO;

public class BannerAdResultEvent extends BaseBackendResultEvent<BannerAdSendEvent>{

	public final int statusCode;
	public final BannerAdDTO data;
	public BannerAdResultEvent(Result result, int statusCode, BannerAdDTO data, BannerAdSendEvent sendEvent) {
		super(result, sendEvent);
		
		this.statusCode = statusCode;
		this.data = data;
		
	}

}
