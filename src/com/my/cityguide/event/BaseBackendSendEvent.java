package com.my.cityguide.event;


public abstract class BaseBackendSendEvent {
	
	public final String pageClass;
	public final String parentPages;
	
	private boolean handled;
	
	public BaseBackendSendEvent(String pageClass, String parentPages) {
		this.pageClass = pageClass;
		this.parentPages = parentPages;
	}
	
	public BaseBackendSendEvent(Class<?> pageClass, String parentPages) {
		if(pageClass!=null)
			this.pageClass = pageClass.getName();
		else
			this.pageClass="";
		this.parentPages = parentPages;
	}
	
	
	
	public boolean isHandled() {
		return handled;
	}
	
	public void setHandled() {
		this.handled = true;
	}
	
	public abstract BaseBackendResultEvent<?> failureEvent();
	

}
