package com.my.cityguide.event;

/**
 * This is the base class which holds the result of certain event. 
 * 
 * @author xichen
 *
 * @param <T>
 */

public class BaseBackendResultEvent<T extends BaseBackendSendEvent> {
	
	public static enum Result { SUCCESS, FAILURE }
	
	public final Result result;
	public final T sendEvent;
	
	public BaseBackendResultEvent(Result result, T sendEvent) {
		this.result = result;
		this.sendEvent = sendEvent;
	}
	
}