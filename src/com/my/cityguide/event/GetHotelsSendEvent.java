package com.my.cityguide.event;

public class GetHotelsSendEvent extends BaseBackendSendEvent{

	public GetHotelsSendEvent(Class<?> pageClass, String parentPages) {
		super(pageClass, parentPages);
	}

	@Override
	public BaseBackendResultEvent<?> failureEvent() {
		return null;
	}

}
