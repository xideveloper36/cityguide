package com.my.cityguide.event;

import com.my.cityguide.data.ServiceInfoDTO;

public class GetServiceInfoResultEvent extends BaseBackendResultEvent<GetServiceInfoSendEvent>{

	public ServiceInfoDTO serviceInfoDTO;
	public GetServiceInfoResultEvent(Result result, ServiceInfoDTO serviceInfoDTO, GetServiceInfoSendEvent sendEvent) {
		super(result, sendEvent);
		this.serviceInfoDTO = serviceInfoDTO;
	}

}
