package com.my.cityguide.event;

public class BackendRequestSentEvent {
public final BaseBackendSendEvent event;
	
	public BackendRequestSentEvent(BaseBackendSendEvent event) {
		this.event = event;
	}
}
