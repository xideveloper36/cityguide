package com.my.cityguide.event;

import java.util.List;

import com.my.cityguide.data.HotelDTO;

public class GetHotelsResultEvent extends BaseBackendResultEvent<GetHotelsSendEvent>{

	public List<HotelDTO> hotels;
	
	public GetHotelsResultEvent(Result result, List<HotelDTO> hotels, GetHotelsSendEvent sendEvent) {
		super(result, sendEvent);
		this.hotels = hotels;
	}

}
