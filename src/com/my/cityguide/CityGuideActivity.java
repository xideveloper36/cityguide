package com.my.cityguide;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.inject.Inject;
import com.my.cityguide.event.NewSessionEvent;
import com.my.cityguide.util.EventObservers;
import com.my.cityguide.util.GuideFragmentsManager;


//@ContentView(R.layout.activity_city_guide)
@ContentView(R.layout.main)
public class CityGuideActivity extends AbstractGuideActivity implements OnItemClickListener{
	private String[] mNavItems;// = {"Option1","Option2","Option3","Optoin4","Option5"};
	
	@InjectView(R.id.drawer_layout) private DrawerLayout drawerLayout;
	@InjectView(R.id.left_drawer) private ListView mDrawerList;
	
	@Inject private EventObservers eventobservers;
	
	@Inject private GuideFragmentsManager guideFragManager;
	
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
    private CharSequence mTitle;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mNavItems = new String[] {"Option1","Option2","Option3","Optoin4","Option5"};
		
		if(mDrawerList!=null){
			Log.v("mDrawerList NOT null","ok");
		}else{
			Log.v("mDrawerList is null","ok");
		}
		mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mNavItems));
		mDrawerList.setOnItemClickListener(this);
		
		mTitle = mDrawerTitle = getTitle();
		
		
		mDrawerToggle = new ActionBarDrawerToggle(
				this,                       //Host Activity
				drawerLayout,				//DrawerLayout object
	            R.drawable.ic_drawer, 		//Nav drawer icon to replace 'Up' caret
	            R.string.drawer_open,       //Open drawer description
	            R.string.drawer_close       //Close drawer description
	            ) {

			            /** Called when a drawer has settled in a completely closed state. */
			            public void onDrawerClosed(View view) {
			                //getActionBar().setTitle(mTitle);
			               // invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			            }
		
			            /** Called when a drawer has settled in a completely open state. */
			            public void onDrawerOpened(View drawerView) {
			                //getActionBar().setTitle(mDrawerTitle);
			               // invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			            }
			        };

	   // Set the drawer toggle as the DrawerListener
	   drawerLayout.setDrawerListener(mDrawerToggle);
	
	   //getActionBar().setDisplayHomeAsUpEnabled(true);
	   //getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		guideFragManager.setActivity(this);
		
		//eventManager.fire(new ClearBackStackEvent());
		eventManager.fire(new NewSessionEvent());
	}
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

	
	
	
	
	/* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.city_guide, menu);
		return true;
	}

	/**
	 * Drawer list item click listener
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		// TODO Auto-generated method stub
		
	}
	
	
}
