package com.my.cityguide.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * This broadcast receiver is registered in AndroidManifest.xml, 
 * it is invoked when system booting process(fired by system) is completed
 * & when power cable is discnnected from device
 * @author xichen
 *
 */
public class SystemIntentReceiver extends BroadcastReceiver{

	/**
	 * overriding the onReceive() method where each message is received as a Intent object parameter
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		
		String message = "System broadcast intent detected: "+intent.getAction();
		if(intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")){
			message = "木有电了。。咪咪咪!";
		}else if(intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")){
			message = "来电了！！！吼吼吼!";
		}
		
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

}
