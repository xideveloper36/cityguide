package com.my.cityguide.protocol;

import roboguice.event.Observes;

import com.my.cityguide.data.BannerAdDTO;
import com.my.cityguide.event.BannerAdResultEvent;
import com.my.cityguide.event.BannerAdSendEvent;
import com.my.cityguide.event.BaseBackendResultEvent.Result;

public class BannerAdReq extends BasePack<BannerAdSendEvent>{
	
	
	
	public void onAdInformationSend(@Observes BannerAdSendEvent sendEvent){
		
		/**
		 * use HTTP GET to fetch the advertisement info
		 */
		this.get(backendBaseUrl+"banner", sendEvent);
		
		
	}

	@Override
	protected void onSuccess(int responseCode, String response, BannerAdSendEvent sendEvent) {
		eventManager.fire(new BannerAdResultEvent(Result.SUCCESS, responseCode, unpack(responseCode, response), sendEvent));
		
	}

	@Override
	protected void onFailure(int statusCode, Throwable error, String content, BannerAdSendEvent sendEvent) {
		eventManager.fire(new BannerAdResultEvent(Result.FAILURE, statusCode, null, sendEvent));
	}
	
	private BannerAdDTO unpack(int code, String response){
		return gson.fromJson(response, BannerAdDTO.class);
	}

}
