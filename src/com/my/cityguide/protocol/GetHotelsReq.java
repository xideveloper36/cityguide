package com.my.cityguide.protocol;

import java.util.List;

import roboguice.event.Observes;

import com.my.cityguide.data.HotelDTO;
import com.my.cityguide.event.GetHotelsResultEvent;
import com.my.cityguide.event.GetHotelsSendEvent;
import com.my.cityguide.event.BaseBackendResultEvent.Result;
import com.google.gson.reflect.TypeToken;

public class GetHotelsReq extends BasePack<GetHotelsSendEvent>{

	public void onGetHotels(@Observes GetHotelsSendEvent sendEvent){
		//Send HTTP GET request for hotels
		get(backendBaseUrl+"hotels", sendEvent);
	}
	
	@Override
	protected void onSuccess(int responseCode, String response, GetHotelsSendEvent sendEvent) {
		eventManager.fire(new GetHotelsResultEvent(Result.SUCCESS, unpack(responseCode, response), sendEvent));
	}

	@Override
	protected void onFailure(int statusCode, Throwable error, String content, GetHotelsSendEvent sendEvent) {
		
	}
	
	/*
	 * Assume server response is in JSON format:
	 *e.g. {{"id": 1,"name":"Nordic Hotel","price":50.50,"address":""},
	         {"id": 2,"name":"Hotel Arthur","price":50.50,"address":""}}
	 */
	
	private List<HotelDTO> unpack(int code, String response){
		TypeToken<List<HotelDTO>> typeToken = new TypeToken<List<HotelDTO>>(){};
		return gson.fromJson(response, typeToken.getType());
	}

}
