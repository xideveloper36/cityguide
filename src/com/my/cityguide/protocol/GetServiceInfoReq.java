package com.my.cityguide.protocol;

import roboguice.event.Observes;

import com.my.cityguide.data.ServiceInfoDTO;
import com.my.cityguide.event.BaseBackendResultEvent.Result;
import com.my.cityguide.event.GetServiceInfoResultEvent;
import com.my.cityguide.event.GetServiceInfoSendEvent;

public class GetServiceInfoReq extends BasePack<GetServiceInfoSendEvent>{
	
	public void onGetServiceInfo(@Observes GetServiceInfoSendEvent event){
		get(baseURL+"serviceinfo?lang=en", event);
		//get("http://www.google.com/contact", event);
	}


	@Override
	protected void onSuccess(int responseCode, String response,
			GetServiceInfoSendEvent sendEvent) {
		eventManager.fire(new GetServiceInfoResultEvent(Result.SUCCESS, parseResponse(response), sendEvent));
	}

	@Override
	protected void onFailure(int statusCode, Throwable error, String content,
			GetServiceInfoSendEvent sendEvent) {
		
	}
	
	private ServiceInfoDTO parseResponse(String jsonResponse){
		return gson.fromJson(jsonResponse, ServiceInfoDTO.class);
	}

}
