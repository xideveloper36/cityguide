package com.my.cityguide.protocol;

import roboguice.event.EventManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.my.cityguide.event.BackendRequestSentEvent;
import com.my.cityguide.event.BaseBackendSendEvent;
import com.my.cityguide.http.AsyncHttpClient;
import com.my.cityguide.http.AsyncHttpResponseHandler;

/**
 * This class uses the HTTP client instance to call the HTTP verb methods
 * @author xichen
 *
 * @param <T>
 */
public abstract class BasePack <T extends BaseBackendSendEvent>{
	
	protected final String baseURL = "http://gloriole.saunalahti.fi/credit/rest/";
	protected String backendBaseUrl = "http://my.server.url/rest/";
	
	@Inject protected EventManager eventManager;
	@Inject protected AsyncHttpClient mHttpClient;
	
	protected final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

	protected abstract void onSuccess(int responseCode, String response, T sendEvent);
	protected abstract void onFailure(int statusCode, Throwable error, String content, T sendEvent);

	
	
	protected void get(String url, T event) {
		Log.v("url", url);
		mHttpClient.get(url,  new ResponseHandler(event));
		
		//The observer of this event should pop up a spinner dialog, so that user knows the request is on going.
		eventManager.fire(new BackendRequestSentEvent(event));
	}

	protected void post(String url, String body, T event) {
		//TODO: HTTP post
	}

	protected void put(String url, String body, T event){
		//TODO: HTTP put
		
	}

	protected void delete(String url, T event) {
		//TODO: HTTP delete
	}
	
	/**
	 * HTTP reponse handler
	 * 
	 * A closure for handling the response. from the HTTP client.
	 * Remembers the originating send event, and handles the authentication requested
	 * response.
	 * @author xichen
	 *
	 */
	private class ResponseHandler extends AsyncHttpResponseHandler {

		private final T originatingEvent;

		protected ResponseHandler(T originatingEvent) {
			this.originatingEvent = originatingEvent;
		}
		
		@Override
		public void onSuccess(int responseCode, String response) {
			BasePack.this.onSuccess(responseCode, response, originatingEvent);
		}
		
		@Override
		public void onFailure(int statusCode, Throwable error, String content) {			
			BasePack.this.onFailure(statusCode, error, content, originatingEvent);
			
		}
		
	}

}
