package com.my.cityguide.data;

import java.io.Serializable;

public class ServiceInfoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String terms;
	private String prices;
	private String security;
	private String faq;
	private String customerCare;
	private String forum;
	private String sessionTimeoutInMins;
	private String minimumAge;
	
	public String getTerms() {
		return terms;
	}
	
	public String getPrices() {
		return prices;
	}

	public String getSecurity() {
		return security;
	}
	
	public String getFaq() {
		return faq;
	}
	
	public String getCustomerCare() {
		return customerCare;
	}

	public String getForum() {
		return forum;
	}
	
	public String getSessionTimeoutInMins() {
		return sessionTimeoutInMins;
	}
	
	public String getMinimumAge() {
		return minimumAge;
	}
}
