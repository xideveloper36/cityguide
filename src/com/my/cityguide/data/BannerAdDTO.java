package com.my.cityguide.data;

import java.io.Serializable;

public class BannerAdDTO implements Serializable{
private static final long serialVersionUID = 1L;
	
	private String imageURL;
	private String clickURL;
	
	public String getImageURL() {
		return imageURL;
	}
	public String getClickURL() {
		return clickURL;
	}
}
