package com.my.cityguide.fragments;

import roboguice.inject.InjectView;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.my.cityguide.R;
import com.my.cityguide.aidl.IFoodService;
import com.my.cityguide.services.RemoteFoodService;
import com.my.cityguide.views.MyCircleView;

public class DrawCircleFragment extends AbstractGuideFragment{
	private MyCircleView myCircleView;
	
	@InjectView(R.id.changeAttrBtn) private Button changeAttrBtn;
	
	private IFoodService mIFoodService; //aidl defined interface
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.circle_layout, null);
		myCircleView = (MyCircleView) content.findViewById(R.id.circle_view);
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		changeAttrBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view){
				
				btnPressed(view);
			}
		});
		
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		/*
		 * bind RemoteFoodService which return a mBinder of AIDL defined interface implementation 
		 * so that the method of IFoodService can be accessed
		 */
		getActivity().bindService(new Intent(getActivity(), 
									RemoteFoodService.class), mConnection, 
									Context.BIND_AUTO_CREATE);
	}
	
	@Override
	public void onPause(){
		super.onPause();
		getActivity().unbindService(mConnection);
	}
	
	//Defined in Button of xml layout file
	public void btnPressed(View view){
		//update the View
		//Let's use the set methods defined in MyCircleView to update the custom View appearance
		myCircleView.setCircleColor(Color.GREEN);
		myCircleView.setLabelColor(Color.MAGENTA);
		myCircleView.setLabelText("Help");
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {

		/*
		 * When the client receives the IBinder in the onServiceConnected() callback, 
		 * it must call YourServiceInterface.Stub.asInterface(service) to cast the returned parameter to YourServiceInterface type.
		 */
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			mIFoodService = IFoodService.Stub.asInterface(service);
			Log.v("=================", "====================");
			try {
				Toast.makeText(getActivity(), "From AIDL interface impl.: "+mIFoodService.getPid(), Toast.LENGTH_SHORT).show();
				Log.v("IFoodService.getPid()", mIFoodService.getPid()+"");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mIFoodService = null;
			
		}
		
	};

}
