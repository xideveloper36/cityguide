package com.my.cityguide.fragments.concert;

import roboguice.event.EventThread;
import roboguice.event.Observes;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.my.cityguide.R;
import com.my.cityguide.data.ServiceInfoDTO;
import com.my.cityguide.event.GetServiceInfoResultEvent;
import com.my.cityguide.event.GetServiceInfoSendEvent;
import com.my.cityguide.fragments.AbstractGuideFragment;

public class ConcertFragment extends AbstractGuideFragment{
	
	@InjectView(R.id.response_data) private TextView responseDataTV;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		
		View content = inflater.inflate(R.layout.concert, null); 
		
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	
	}
	
	@Override
	public void onStart(){
		super.onStart();
		eventManager.fire(new GetServiceInfoSendEvent(ConcertFragment.class, null));
	}
	
	public void onGetServiceInfoResult(@Observes(EventThread.UI) GetServiceInfoResultEvent event){
		switch(event.result){
		case SUCCESS:
			ServiceInfoDTO serviceInfoDTO = event.serviceInfoDTO;
			Log.v("*UI-service terms*", serviceInfoDTO.getTerms());
			responseDataTV.setText(serviceInfoDTO.getTerms());
			break;
		case FAILURE:
			break;
		}
	}

}
