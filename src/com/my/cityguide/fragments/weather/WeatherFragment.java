package com.my.cityguide.fragments.weather;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.google.inject.Inject;
import com.my.cityguide.R;
import com.my.cityguide.fragments.AbstractGuideFragment;
import com.my.cityguide.util.DummyTabContent;
import com.my.cityguide.util.GuideFragmentsManager;

public class WeatherFragment extends AbstractGuideFragment implements OnTabChangeListener{
	@Inject protected GuideFragmentsManager guideFragMgr;
	private TabHost mTabHost;
	private Resources resources;
	private View content;
	private Context baseContext;
	
	private final String TAB_DAY_WEATHER = "dayWeather";
	private final String TAB_WEEK_WEATHER = "weekWeather";
	private final String TAB_MONTH_WEATHER = "monthWeather";
	
	private final String[] tabTags = {"dayWeather","weekWeather","monthWeather"};
	private final Class[] fragmentClasses = {DayWeatherFragment.class, WeekWeatherFragment.class, MonthWeatherFragment.class};
	
	@Override
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState); 
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		resources = activity.getResources();
		content = inflater.inflate(R.layout.weather, null); 
		baseContext = activity.getBaseContext();
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
			mTabHost = (TabHost) content.findViewById(android.R.id.tabhost);
			
	        mTabHost.setup();
	        mTabHost.setOnTabChangedListener(this);
	        
	       //Add tabs to UI
	        newTab(TAB_DAY_WEATHER, "Day Weather", R.drawable.android_selector);
	        newTab(TAB_WEEK_WEATHER, "Week Weather", R.drawable.apple_selector);
	        newTab(TAB_MONTH_WEATHER, "Month Weather", R.drawable.android_selector);
	       	      
	}
	
	/**Defining tab builder for Andriod tab & add the tab to TabHost**/
	private void newTab(String tabId, String tabLabel, int tabIconResource){
		
        TabSpec tabSpec = mTabHost.newTabSpec(tabId);
        tabSpec.setIndicator(tabLabel, resources.getDrawable(tabIconResource));//set label & icon of the tab
        tabSpec.setContent(new DummyTabContent(baseContext)); //set the page content of the tab, we use dummy(blank) content here
        mTabHost.addTab(tabSpec);
        
	}

	@Override
	public void onTabChanged(String tabId) {
		Log.v("*onTabChanged, tabId=*", tabId);
		guideFragMgr.updateFragmentAsTabContent(tabTags, fragmentClasses, tabId, R.id.realtabcontent);
		
	}

}
