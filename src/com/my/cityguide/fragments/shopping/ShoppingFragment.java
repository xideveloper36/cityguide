package com.my.cityguide.fragments.shopping;

import java.util.List;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.my.cityguide.R;
import com.my.cityguide.broadcastreceiver.CustomIntentReceiver;
import com.my.cityguide.fragments.AbstractGuideFragment;

public class ShoppingFragment extends AbstractGuideFragment{
	
	@InjectView(R.id.broadcast_btn)
	private Button broadcastBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.shopping, null);
		
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		/**
		 * When button pressed, it will broadcast our custom intent "com.my.cityguide.CUSTOM_INTENT" 
		 * which will be intercepted by our registered BroadcastReceiver i.e. CustomIntentReceiver 
		 */
		broadcastBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				broadcastCustomIntent(v);
			}
		});
	}
	
	@Override
	public void onStart(){
		super.onStart();
		/**
		 * Get running tasks(apps) & running processes
		 */
		ActivityManager am = (ActivityManager) activity.getSystemService(Activity.ACTIVITY_SERVICE);
		List<RunningTaskInfo> runningTasks = am.getRunningTasks(6);
		for(RunningTaskInfo task : runningTasks){
			Log.v("*Running task:*", task.topActivity.getPackageName());//get current active app
		}
		List<RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
		for(RunningAppProcessInfo processInfo: runningProcesses){
			Log.v("*Running Process:*", processInfo.processName);
		}
		/**
		 * ****************************************************************************
		 */
	}
	
	/**
	 * Broadcast a custom intent
	 */
	public void broadcastCustomIntent(View view){
		//explicitly set the receiver class here, so that in AndroidManifest.xml when registering the receiver,
		//there is no need to include <intent-filter> tags
		Intent intent = new Intent(activity, CustomIntentReceiver.class);  
		//intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		intent.setAction("com.my.cityguide.CUSTOM_INTENT");
		activity.sendBroadcast(intent);
	}

}
