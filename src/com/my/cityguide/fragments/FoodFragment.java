package com.my.cityguide.fragments;

import roboguice.inject.InjectView;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.my.cityguide.R;
import com.my.cityguide.services.MessengerService;
import com.my.cityguide.services.MyBinderService;
import com.my.cityguide.services.MyBinderService.MyBinder;
import com.my.cityguide.services.MyIntentService;

public class FoodFragment extends AbstractGuideFragment{
	
	@InjectView(R.id.asycTaskBtn) private Button asyncTaskBtn;
	@InjectView(R.id.progressbar_Horizontal) private ProgressBar progressBar;
	
	@InjectView(R.id.startIntentServiceBtn) private Button startIntentServiceBtn;
	@InjectView(R.id.bindServiceBtn) private Button bindServiceBtn;
	@InjectView(R.id.messengerServiceBtn) private Button messengerServiceBtn;
	
	private MyBinderService myBinderService;
	boolean mBound = false;
	
	/** Messenger for communicating with the service. */
	Messenger messenger=null;
	/** Flag indicating whether we have called bind on the service. */
	boolean mBound2 = false;
	
	class ClientIncomingHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			switch(msg.what){
			case MessengerService.MSG_SHOW_TOAST:
				Toast.makeText(activity, "Rely msg from messenger service", Toast.LENGTH_LONG).show();
				break;
			default:
				super.handleMessage(msg);
			
			}
			Log.v("*Got it in client's handler*",msg.what+" ok");
		}
	}
	
	final Messenger mClientMessenger = new Messenger(new ClientIncomingHandler());
	
	                                                 //<Params, Progress, Result>
	private class BackgroundAsyncTask extends AsyncTask<Void, Integer, Void>{
		
		int myProgress;
		
		@Override
		protected void onPreExecute(){
			myProgress = 0;
			progressBar.setProgress(myProgress);
		}

		@Override
		protected Void doInBackground(Void... params) {
			while(myProgress<100){
				myProgress++;
				//each call to this method will trigger the execution of onProgressUpdate(Progress...) on the UI thread
				publishProgress(myProgress);
				SystemClock.sleep(100);
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Integer...values){
			progressBar.setProgress(values[0]);
		}
		
		@Override
		protected void onPostExecute(Void result){
			progressBar.setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.food, null);
		
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		progressBar.setVisibility(View.INVISIBLE);
		
		asyncTaskBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				progressBar.setVisibility(View.VISIBLE);
				new BackgroundAsyncTask().execute();
			}
		});
		
		startIntentServiceBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				
				/*??
				 * if you want the service to send a result back, then the client that starts the service can create 
				 * a PendingIntent for a broadcast (with getBroadcast()) and deliver it to the service in the Intent 
				 * that starts the service. The service can then use the broadcast to deliver a result.
				 */
				Intent intent = new Intent(activity, MyIntentService.class);
				activity.startService(intent);
			}
		});
		
		bindServiceBtn.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View arg0) {
				if(mBound){
					// Call a method from the MyBinderService.
		            // However, if this call were something that might hang, then this request should
		            // occur in a separate thread to avoid slowing down the activity performance.
					int num = myBinderService.getRandomNumber();
					Toast.makeText(activity, "number: " + num, Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
		messengerServiceBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(!mBound2) return;
				//Create & send message to the messenger service, using a supported 'what' value
				Message msg = Message.obtain(null, MessengerService.MSG_SAY_HELLO, 0, 0);
				//If get any reply to this msg from the messenger service, mClientMessenger will handle the reply
				msg.replyTo = mClientMessenger;
				try{
					messenger.send(msg);
					
					
				}catch(RemoteException e){
					e.printStackTrace();
				}
			}
		});
		
	}
	
	@Override
	public void onStart(){
		super.onStart();
		Log.v("*FoodFragment-onStart()*","ok");
		//Bind to myBinderSerivce
		Intent intent = new Intent(activity, MyBinderService.class);	
		activity.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
		
		//Bind to messenger service
		Intent intent2 = new Intent(activity, MessengerService.class);
		activity.bindService(intent2, serviceConnection2, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	public void onStop(){
		super.onStop();
		//Unbind from myBinderService
		if(mBound){
			activity.unbindService(serviceConnection);
			mBound = false;
		}
		
		//Unbind messenger service
		if(mBound2){
			activity.unbindService(serviceConnection2);
			mBound2 = false;
		}
	}
	
	
	/**BINDER SERVICE
	 * bindService() method returns immediately without a value, but when the Android system creates the connection between the client and service, 
	 * it calls onServiceConnected() on the ServiceConnection, to deliver the IBinder that the client can use to communicate with the service.
	 */
	/** Defines callbacks for service binding, passed to bindService() */
	private ServiceConnection serviceConnection = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName compName, IBinder iBinder) {
			Log.v("*On Service Connected*","ok");
			// We've bound to MyBinderService, cast the IBinder and get MyBinderService instance
			MyBinder myBinder = (MyBinder) iBinder;
			myBinderService = myBinder.getService();
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBound = false;
			
		}
		
	};
	
	/**MESSENGER SERVICE
     * Class for interacting with the main interface of the service.
     */
	private ServiceConnection serviceConnection2 = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName compName, IBinder iBinder) {
			// This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
			messenger = new Messenger(iBinder);
			mBound2 = true;	
			
		}

		@Override
		public void onServiceDisconnected(ComponentName compName) {
			// This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
			//This is NOT called when client unbind the service.
			messenger = null;
			mBound2 = false;
			
		}
		
	};

}
