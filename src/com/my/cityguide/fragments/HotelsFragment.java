package com.my.cityguide.fragments;

import java.util.ArrayList;
import java.util.List;

import roboguice.event.EventThread;
import roboguice.event.Observes;
import roboguice.inject.InjectView;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.my.cityguide.R;
import com.my.cityguide.data.HotelDTO;
import com.my.cityguide.event.GetHotelsResultEvent;
import com.my.cityguide.event.GetHotelsSendEvent;
import com.my.cityguide.util.HotelsAdapter;
import com.my.cityguide.views.LinearListView;
import com.my.cityguide.views.LinearListView.OnItemClickListener;


public class HotelsFragment extends AbstractGuideFragment{
	
	//!for some reason, @InjectView(R.id.xx) does not work with customized View element
	
	@InjectView(R.id.page_info) private TextView pageInfo;
	@InjectView(R.id.threadANDhandler) private Button threadAndHandlerBtn;
	
	private ProgressDialog progressDialog;
	
	private LinearListView hotelListView;
	private List<HotelDTO> hotelList;
	
	
	/*
	 * The class Handler can update the UI.
	 * A handle provides methods for receiving messages and for runnables. 
	 * To use a handler you have to subclass it and override handleMessage() to process messages. 
	 * To process runnables you can use the method post(); You only need one instance of a handler in your activity/fragment.
	 */
	private Handler messageHandler = new Handler(){
		
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle b1 = msg.getData();
			pageInfo.setText(b1.getString("message"));
			progressDialog.dismiss();
		}
		
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.hotels, null);
		hotelListView = (LinearListView) content.findViewById(R.id.hotel_list);
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		threadAndHandlerBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				runInBackground();
			}
		});
		
	}
	
	private void runInBackground(){
		this.progressDialog = ProgressDialog.show(activity, "", "Doing...");
		new Thread(){
			public void run(){

				//Implementing your long running operations
				try{
					Thread.sleep(1000);
				}catch(InterruptedException e){
					Log.v("**InterruptedException","ok");
				}
				Message msg = Message.obtain();
				prepareMsg(msg);
				//The messageHandler.sendMessage(m) is the callback on the parent thread(Main UI Thread)'s messageHandler to 
				//inform that the child thread has finished its work or you can also send empty message through the messagehandler. i.e messageHandler.sendEmptyMessage(0)
				messageHandler.sendMessage(msg);
			}//end of run()
		}.start();
	}
	
	private void prepareMsg(Message msg){
		Bundle bundle = new Bundle();
		bundle.putString("message", "Done in background!!!");
		msg.setData(bundle);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		
		/* Uncomment the following code to show hard coded data to show hotel list on the screen
		 */
		/*
		hotelList = getHotels(); 
		HotelsAdapter hotelsAdapter = new HotelsAdapter(activity, hotelList, R.layout.hotel_row);
		hotelListView.setAdapter(hotelsAdapter);
		*/
		eventManager.fire(new GetHotelsSendEvent(HotelsFragment.class, ""));
	}
	
	
	/*
	 * Show backend responsed hotel list
	 */
	public void onGetHotelsResult(@Observes(EventThread.UI) GetHotelsResultEvent event){
		switch(event.result){
		case SUCCESS:
			hotelList = event.hotels; //Use backend responsed data
			HotelsAdapter hotelsAdapter = new HotelsAdapter(activity, hotelList, R.layout.hotel_row);
			hotelListView.setAdapter(hotelsAdapter);
			
			hotelListView.setOnItemClickListener(new OnItemClickListener(){
				public void onItemClick(LinearListView parent, View view, int position, long id) {
					HotelDTO hotel = hotelList.get(position);
					showToast("You selected: "+hotel.getName());
				}
			});
			
			break;
		case FAILURE:
			break;
		}
	}
	
	//This method is used to show hard coded hotel list, 
	private List<HotelDTO> getHotels(){
		List<HotelDTO> hotelList = new ArrayList<HotelDTO>();
		for(int i=0; i<6; i++){
			HotelDTO hotel = new HotelDTO();
			hotel.setId(i);
			hotel.setName("Hotel "+ String.valueOf(i));
			hotel.setAddress("Address");
			hotel.setPrice(i*10.00);
			hotelList.add(hotel);
		}
		return hotelList;
	}
}
