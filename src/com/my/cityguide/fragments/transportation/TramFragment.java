package com.my.cityguide.fragments.transportation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.my.cityguide.R;
import com.my.cityguide.fragments.AbstractGuideFragment;

public class TramFragment  extends AbstractGuideFragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.transport_tram, null);
		
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

}
