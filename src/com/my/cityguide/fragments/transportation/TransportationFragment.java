package com.my.cityguide.fragments.transportation;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.my.cityguide.R;
import com.my.cityguide.fragments.AbstractGuideFragment;
import com.my.cityguide.fragments.weather.DayWeatherFragment;
import com.my.cityguide.fragments.weather.MonthWeatherFragment;
import com.my.cityguide.fragments.weather.WeekWeatherFragment;

public class TransportationFragment extends AbstractGuideFragment implements OnTabChangeListener{
	
	private static final String TAG = "FragmentTabs";
	
	private final String TAB_BUS = "bus_transport";
	private final String TAB_TRAM = "tram_transport";
	private final String TAB_FERRY = "ferry_transport";
    
    @InjectView(android.R.id.tabhost) private TabHost mTabHost;
    
    private View rootView;
    //private TabHost mTabHost;
    private int mCurrentTab;
	
	@Override
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.public_transportation, null); 
		rootView = content;
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		
        setupTabs();
	}
	
	
	private void setupTabs() {
        mTabHost.setup(); // you must call this before adding your tabs!
        
        mTabHost.setOnTabChangedListener(this);
        mTabHost.setCurrentTab(mCurrentTab);
        
        mTabHost.addTab(newTab(TAB_BUS, R.string.bus, R.id.tab_bus));
        mTabHost.addTab(newTab(TAB_TRAM, R.string.tram, R.id.tab_tram));
        mTabHost.addTab(newTab(TAB_FERRY, R.string.ferry, R.id.tab_ferry));
    }
	
	/**
	 * A tab has a tab indicator, content & a tag, this builder helps to choose among them
	 * We use customized layout for the tab button (R.layout.my_tab)
	 
	 */
	private TabSpec newTab(String tag, int labelId, int tabContentId) {
        Log.d(TAG, "buildTab(): tag=" + tag);
 
        //Customized View for tab  
        View indicator = LayoutInflater.from(getActivity()).inflate(
							                R.layout.my_tab,
							                (ViewGroup) rootView.findViewById(android.R.id.tabs), 
							                false);
        
        ((TextView) indicator.findViewById(R.id.text)).setText(labelId);
 
        TabSpec tabSpec = mTabHost.newTabSpec(tag);
        tabSpec.setIndicator(indicator);//set customized view to each tab
        tabSpec.setContent(tabContentId);
        return tabSpec;
    }
	
	@Override
    public void onTabChanged(String tabId) {
		Log.v("onTabChanged()2: tabId=", tabId);
		
		if (TAB_BUS.equals(tabId)) {
			guideFragMgr.updateSpecificFragmentAsTabContent(TAB_BUS, BusFragment.class, tabId, R.id.tab_bus);
            mCurrentTab = 0;
            
        }
        if (TAB_TRAM.equals(tabId)) {
        	guideFragMgr.updateSpecificFragmentAsTabContent(TAB_TRAM, TramFragment.class, tabId, R.id.tab_tram);
            mCurrentTab = 1;
           
        }
        if (TAB_FERRY.equals(tabId)) {
        	guideFragMgr.updateSpecificFragmentAsTabContent(TAB_FERRY, FerryFragment.class, tabId, R.id.tab_ferry);
            mCurrentTab = 2;
           
        }
        

    }
 

}
