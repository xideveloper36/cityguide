package com.my.cityguide.fragments;

import roboguice.event.EventManager;
import roboguice.fragment.RoboFragment;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.google.inject.Inject;
import com.my.cityguide.CityGuideActivity;
import com.my.cityguide.util.GuideFragmentsManager;

/**
 * The abstract fragment of the application, that implements all the common
 * part of the fragments.
 * 
 * @author Xi
 * 
 */

public class AbstractGuideFragment extends RoboFragment{
	
	@Inject protected GuideFragmentsManager guideFragMgr;
	@Inject protected EventManager eventManager;
	
	
    
	
	protected CityGuideActivity activity;
	
	public CityGuideActivity getGuideActivity() {
		return this.activity;
	}

	public void setGuideActivity(CityGuideActivity activity) {
		this.activity = activity;
	}
	
	/**
	 * Called by every fragment to get the instance of activity
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		setGuideActivity((CityGuideActivity) activity);
	}

	
	protected void showToast(String msg){
		Context context = activity.getApplicationContext();
		CharSequence text = msg;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
	
	//Switch to the target fragment on UI
	protected void switchFragment(Class<? extends AbstractGuideFragment> target, boolean putOnStack, Bundle bundle) {
		guideFragMgr.changeToFragment(target, putOnStack, bundle);
	}
	
	protected int getSizeInDP(int sizeInPx){
		float scale = getResources().getDisplayMetrics().density;
		return (int) (sizeInPx*scale + 0.5f);
	}
	
}
