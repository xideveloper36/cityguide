package com.my.cityguide.fragments;

import roboguice.event.EventThread;
import roboguice.event.Observes;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.my.cityguide.R;
import com.my.cityguide.event.BannerAdResultEvent;
import com.my.cityguide.event.BannerAdSendEvent;
import com.my.cityguide.fragments.concert.ConcertFragment;
import com.my.cityguide.fragments.shopping.ShoppingFragment;
import com.my.cityguide.fragments.transportation.TransportationFragment;
import com.my.cityguide.fragments.weather.WeatherFragment;
import com.my.cityguide.views.AdBanner;

public class FrontpageFragment extends AbstractGuideFragment{

	@InjectView(R.id.frontpage_food) private View food;
	@InjectView(R.id.frontpage_hotel) private View hotel;
	@InjectView(R.id.frontpage_museum) private View museum;
	@InjectView(R.id.frontpage_transport) private View publicTransport;
	@InjectView(R.id.frontpage_weather) private View weather;
	@InjectView(R.id.frontpage_concert) private View concert;
	@InjectView(R.id.frontpage_shopping)	private View shopping;
	
	@InjectView(R.id.frontpage_banner) private AdBanner adBanner;
	
	private Bundle bundle = new Bundle();
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	
		View content = inflater.inflate(R.layout.frontpage, null); 
		
		return content;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		setListeners();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		//sendRequest();
		//loadAdBanner(adBanner);
	}
	
	private void sendRequest(){
		//TODO: send other requests
		//e.g. eventManager.fire(new GetUserInfoSendEvent(this.getClass(), ""));
	}
	
	private void loadAdBanner(AdBanner ab) {
		this.adBanner = ab;
		if (adBanner != null){
			eventManager.fire(new BannerAdSendEvent(this.getClass(), ""));
		}
	}
	
	public void onGetBannerResult(@Observes(EventThread.UI) BannerAdResultEvent event) {
		
		switch (event.result) {
		case SUCCESS:
			Log.v("Got banner advertisement", "successful");
			
			if(adBanner != null) {
				//TODO: show the banner image
			}		
			break;
		case FAILURE:
			Log.v("Got banner advertisement", "failed");
			if(adBanner!=null){
				adBanner.setDrawable(getResources().getDrawable(R.drawable.adv));
				adBanner.setOnClickListener(null);
			}
			break;
		default:
			break;
		}

	}
	
	private void setListeners(){
		food.setOnClickListener(clickListener);
		hotel.setOnClickListener(clickListener);
		museum.setOnClickListener(clickListener);
		publicTransport.setOnClickListener(clickListener);
		weather.setOnClickListener(clickListener);
		concert.setOnClickListener(clickListener);
		shopping.setOnClickListener(clickListener);
	}
	
	
	private OnClickListener clickListener = new OnClickListener(){
		@Override
		public void onClick(View v){
			switch(v.getId()){
			case R.id.frontpage_food:
				switchFragment(FoodFragment.class, true, bundle);
				break;
			case R.id.frontpage_hotel:
				switchFragment(HotelsFragment.class, true, bundle);
				break;
			case R.id.frontpage_museum:
				switchFragment(DrawCircleFragment.class, true, bundle);
				break;
			case R.id.frontpage_transport:
				switchFragment(TransportationFragment.class, true, bundle);
				break;
			case R.id.frontpage_weather:
				switchFragment(WeatherFragment.class, true, bundle);
				break;
			case R.id.frontpage_concert:
				switchFragment(ConcertFragment.class, true, bundle);
				break;
			case R.id.frontpage_shopping:
				switchFragment(ShoppingFragment.class, true, bundle);
				break;
			}
		}
	};
}
