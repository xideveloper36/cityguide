package com.my.cityguide.services;

import java.util.Random;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * If your service is used only by the local application and does not need to work across processes, 
 * then you can implement your own Binder class that provides your client direct access to
 * public methods in the service. 
 * Two way communication between client & service is achieved.
 * @author xichen
 *
 */
public class MyBinderService extends Service{
	//Binder given to clients
	private final IBinder mBinder = new MyBinder();
	
	//Random nr generator
	private final Random mGenerator = new Random();
	
	/**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
	public class MyBinder extends Binder{
		public MyBinderService getService(){
			return MyBinderService.this;
		}
	}
	
	@Override
	public IBinder onBind(Intent intent){
		return mBinder;
	}
	
	//Method for client
	public int getRandomNumber(){
		return mGenerator.nextInt(10);
	}

}
