package com.my.cityguide.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * -IntentService handle requests in a queue.
 * -IntentService stops the service after all start requests have been handled, 
 * so you never have to call stopSelf().
 * @author xichen
 *
 */
public class MyIntentService extends IntentService{

	public MyIntentService(){
		super("MyIntentService");
	}
	
	/** 
	 * @param name: the name of worker thread.
	   * A constructor is required, and must call the super IntentService(String)
	   * constructor with a name for the worker thread.
	   */

	public MyIntentService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/**
	   * The IntentService calls this method from the default worker thread with
	   * the intent that started the service. When this method returns, IntentService
	   * stops the service, as appropriate.
	   */

	@Override
	protected void onHandleIntent(Intent intent) {
		// Normally we would do some work here, like download a file.
	      // For our sample, we just sleep for 5 seconds.
	      long endTime = System.currentTimeMillis() + 5*1000;
	      while (System.currentTimeMillis() < endTime) {
	          synchronized (this) {
	              try {
	            	  Log.v("*MyIntentService-onHandleIntent()*","ok");
	                  wait(endTime - System.currentTimeMillis());
	              } catch (Exception e) {
	              }
	          }
	      }

		
	}

}
