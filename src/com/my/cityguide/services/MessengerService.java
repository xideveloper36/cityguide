//http://developer.android.com/guide/components/bound-services.html#Messenger
package com.my.cityguide.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

/**
 * If you need your service to communicate with remote processes, 
 * then you can use a Messenger to provide the interface for your service. 
 * This technique allows you to perform interprocess communication (IPC) without the need to use AIDL.
 * 
 *  There are no "methods" for the client to call on the service. 
 *  Instead, the client delivers "messages" (Message objects) that the service receives in its Handler.
 * @author xichen
 *
 */
public class MessengerService extends Service{
	/** Command to the service to display a message */
	public static final int MSG_SAY_HELLO = 1;
	public static final int MSG_SHOW_TOAST = 2;
	/**
	 * Handler is used to create a Messenger which is a reference to the Handler
	 * The service receives each Message in its Handler—specifically, in the handleMessage() method.
	 * 
     * Handler of incoming messages from clients.
     */
	public class IncomingHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			switch(msg.what){
			case MSG_SAY_HELLO:
				//get the instance of client(Fragment/Activity) side messenger defined in client
				Messenger clientMessenger = msg.replyTo;
				//Prepare a reply message to client
				Message msgToClient = Message.obtain(null, MSG_SHOW_TOAST, 0, 0);
				//Send the message to client as an reply
				try {
					clientMessenger.send(msgToClient);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.v("*Got it in service's handler*","ok");
				break;
			default:
				super.handleMessage(msg);
			}
		}
		
	}
	
	/**
     * Target we publish for clients to send messages to IncomingHandler.
     */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	/**
	 * The Messenger creates an IBinder that the service returns to clients from onBind().
	 * 
	 * Clients use the IBinder to instantiate the Messenger (that references the service's Handler), 
	 * which the client uses to send Message objects to the service.
	 *
	 * When binding to the service, we return an interface to our messenger for sending messages to service 
	 */
	
	@Override
	public IBinder onBind(Intent intent) {
		Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT).show();
		return mMessenger.getBinder();
	}
					
}
