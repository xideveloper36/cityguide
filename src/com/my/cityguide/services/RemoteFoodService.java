/** http://developer.android.com/guide/components/aidl.html
 * 
 * The Android SDK tools generate an interface in the Java programming language, based on your .aidl file. 
 * This interface has an inner abstract class named "Stub" that extends Binder and implements methods from your AIDL interface. 
 * You must extend the Stub class and implement the methods.
 * 
 * 
 * Stub is an abstract implementation of its parent interface (IFoodService).
 * 
 * To implement the interface (IFoodService) generated from the .aidl, extend the generated Binder interface (for example, IFoodService.Stub) 
 * and implement the methods inherited from the .aidl file.
 * 
 * Now the mBinder is an instance of the Stub class (a Binder), which defines the RPC interface for the service,
 * this instance is exposed to clients so they can interact with the service. 
 */
package com.my.cityguide.services;

import com.my.cityguide.aidl.IFoodService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class RemoteFoodService extends Service{
	/*
	 * Implement the IFoodService interface for RemoteFoodService
	 */
	public IFoodService.Stub mBinder = new IFoodService.Stub() {
		
		@Override
		public int getPid() throws RemoteException {
			// TODO Auto-generated method stub
			return 3;
		}
		
		@Override
		public String getFood() throws RemoteException {
			// TODO Auto-generated method stub
			return "Pizza";
		}
		
		@Override
		public void callFood(String name, boolean isHot) throws RemoteException {
			// TODO Auto-generated method stub
			
		}
	};
	
	
	@Override
    public void onCreate() {
        super.onCreate();
    }

	
	/*
	 * Expose the IFoodService interface (to clients) for RemoteFoodService by implementing onBind() which(non-Javadoc)
	 * returns an instance of my class that implements the generated "Stub"
	 * 
	 * Now, when a client (such as an activity) calls bindService() to connect to this service (RemoteFoodService), 
	 * the client's onServiceConnected() callback receives the mBinder instance returned by the service's onBind() method.
	 *
	 *The client must also have access to the interface class, so if the client and service are in separate applications, 
	 *then the client's application must have a copy of the .aidl file in its src/ directory 
	 *(which generates the android.os.Binder interface—providing the client access to the AIDL methods).
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		
		return mBinder;
	}

}
