package com.my.cityguide.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.my.cityguide.R;

public class MyCircleView extends View{
	
	//circle and text colors
	private int circleCol, labelCol;
	//label text
	private String circleText;
	//paint for drawing custom view
	private Paint circlePaint;

	public MyCircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		//paint object for drawing in onDraw
		circlePaint = new Paint();
		
		//get the attributes specified in attrs.xml using the name we included
		//This typed array will provide access to the attribute values.
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MyCircleView, 0, 0);
	
		//Let's now attempt to retrieve the attribute values (defined in layout), using a try block in case anything goes wrong
		try{
			circleText = a.getString(R.styleable.MyCircleView_circleLabel);
			circleCol = a.getInteger(R.styleable.MyCircleView_circleColor, 0);//0 is default
		    labelCol = a.getInteger(R.styleable.MyCircleView_labelColor, 0);
		    
		    Log.v("*CircleText is*", circleText);
		}finally{
			a.recycle();
		}
		
	}
	
	@Override
	public void onDraw(Canvas canvas){
		/**
		 * Drow circle
		 */
		//get half of the width and height as we are working with a circle
		int viewWidthHalf = this.getMeasuredWidth()/2;
		int viewHeightHalf = this.getMeasuredHeight()/2;
		
		//get the radius as half of the width or height, whichever is smaller
		//subtract ten so that it has some space around it
		int radius = 0;
		if(viewWidthHalf>viewHeightHalf)
		    radius=viewHeightHalf-10;
		else
		    radius=viewWidthHalf-10;
		
		//set properties for painting
		circlePaint.setStyle(Style.FILL);
		circlePaint.setAntiAlias(true);
		
		//set the paint color using the circle color specified
		//This means that the circle will be drawn with whatever color we listed in the layout XML
		circlePaint.setColor(circleCol);
		
		//Draw the circle
		canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, circlePaint);
		
		/**
		 * Draw text
		 */
		//set the text color using the color specified
		circlePaint.setColor(labelCol);
		
		//set text properties
		circlePaint.setTextAlign(Paint.Align.CENTER);
		circlePaint.setTextSize(50);
		
		//draw the text using the string attribute and chosen properties
		canvas.drawText(circleText, viewWidthHalf, viewHeightHalf, circlePaint);
	}
	
	/**Expose the attributes**/
	//GETTER
	public int getCircleColor(){
	    return circleCol;
	}
	public int getLabelColor(){
	    return labelCol;
	}
	public String getLabelText(){
	    return circleText;
	}
	//SETTER
	public void setCircleColor(int newColor){
	    //update the instance variable
	    circleCol=newColor;
	    //redraw the view
	    invalidate();
	    requestLayout();
	}
	public void setLabelColor(int newColor){
	    //update the instance variable
	    labelCol=newColor;
	    //redraw the view
	    invalidate();
	    requestLayout();
	}
	public void setLabelText(String newLabel){
	    //update the instance variable
	    circleText=newLabel;
	    //redraw the view
	    invalidate();
	    requestLayout();
	}
	

}
