package com.my.cityguide.views;

import com.my.cityguide.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

public class PieChart extends View{
	//The attributes
	boolean mShowText;
	int mTextPos;
	
	public PieChart(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		/**
		 * Apply custom attributes
		 */
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PieChart, 0, 0);
		
		try{
			mShowText = a.getBoolean(R.styleable.PieChart_showText, false);
			mTextPos = a.getInteger(R.styleable.PieChart_labelPosition, 0);
		}finally{
			/**
			 * Note that TypedArray objects are a shared resource and must be recycled after use.
			 */
			a.recycle();
		}
	}

	/**A good rule to follow is to always expose any property that affects the visible appearance or behavior of your custom view.**/
	  
	 /** 
	  Expose the property called 'showText'.
	 * @return
	 */
	public boolean isShowText() {
	   return mShowText;
	}

	public void setShowText(boolean showText) {
	   mShowText = showText;
	   /*These following calls are crucial to ensure that the view behaves reliably. 
	    * You have to invalidate the view after any change to its properties that might change its appearance, so that the system knows that it needs to be redrawn. 
	    * Likewise, you need to request a new layout if a property changes that might affect the size or shape of the view. 
	    * !!Forgetting these method calls can cause hard-to-find bugs.*/
	   invalidate();
	   requestLayout();
	}
	
	/**
	 * Expose the property called 'labelPosition'
	 */
	public int getLabelPosition(){
		return mTextPos;
	}
	
	public void setLabelPosition(int textPos){
	   mTextPos = textPos;	
	}
	
}
