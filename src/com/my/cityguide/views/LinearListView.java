package com.my.cityguide.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.LinearLayout;

/**
 * A replacement for {@link android.widget.ListView} that, instead of being scrollable, has the list items in a {@link LinearLayout}. As
 * opposed to ListView, can be used inside scroll views, since it doesn't provide its own scrolling.
 * 
 * Note: Remember to specify android:orientation for the view (since it is based on LinearLayout)
 *  
 * @author xichen
 * 
 */
public class LinearListView extends LinearLayout implements OnClickListener {

	private Adapter adapter;
	private Observer observer = new Observer(this);
	private OnItemClickListener onItemClickListener;

	public LinearListView(Context context) {
		super(context);
	}

	public LinearListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LinearListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setAdapter(Adapter adapter) {
		if (this.adapter != null) {
			this.adapter.unregisterDataSetObserver(observer);
		}
		this.adapter = adapter;
		adapter.registerDataSetObserver(observer);
		observer.onChanged();
	}

	public void setOnItemClickListener(OnItemClickListener listener) {
		this.onItemClickListener = listener;
	}

	@Override
	public void onClick(View v) {
		if (this.onItemClickListener != null) {
			int childIndex = indexOfChild(v);
			
			Log.v("*Child Idex*", String.valueOf(childIndex));
			
			if (childIndex >= 0) {
				this.onItemClickListener.onItemClick(this, v, childIndex, v.getId());
			}
		}
	}

	private static class Observer extends DataSetObserver {
		private LinearListView context;

		public Observer(LinearListView context) {
			this.context = context;
		}

		@Override
		public void onChanged() {
			List<View> oldViews = removeViews();

			Iterator<View> iter = oldViews.iterator();
			for (int i = 0; i < context.adapter.getCount(); i++) {
				View convertView = iter.hasNext() ? iter.next() : null;
				View newView = context.adapter.getView(i, convertView, context);
				newView.setOnClickListener(context);
				context.addView(newView);
			}
			super.onChanged();
		}

		@Override
		public void onInvalidated() {
			removeViews();
			super.onInvalidated();
		}

		private List<View> removeViews() {
			List<View> removedViews = new ArrayList<View>(context.getChildCount());
			for (int i = 0; i < context.getChildCount(); i++) {
				View child = context.getChildAt(i);
				child.setOnClickListener(null);
				removedViews.add(child);
			}
			context.removeAllViews();
			return removedViews;
		}
	}

	public static interface OnItemClickListener {
		public void onItemClick(LinearListView parent, View view, int position, long id);
	}

}

