package com.my.cityguide.views;

import roboguice.event.EventManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.my.cityguide.CityGuideActivity;
import com.my.cityguide.R;

public class AdBanner extends LinearLayout{
	
	private ImageView iv;
	
	protected CityGuideActivity activity;
	
	public AdBanner(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(VERTICAL);
		setGravity(Gravity.BOTTOM);

		View view = LayoutInflater.from(context).inflate(R.layout.ad_banner, this, true);
		iv = (ImageView) view.findViewById(R.id.adv_image);
	}
	
	public void setPicture(Bitmap bm){
		iv.setImageBitmap(bm);
	}
	
	public void setDrawable(Drawable dr){
		iv.setImageDrawable(dr);
	}
	
	public void setClickable(final String adUrl){
		iv.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(adUrl));
				activity.startActivity(browserIntent);
			}
			
		});
	}
}
